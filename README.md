# mat-button-fn

Componente de button utilizando o angular material

## Instalação projeto local

- Retirar do package.json a referência da lib (EM CASO DE CLONE INICIAL)

`@finchildren/mat-button-fn": "file:dist/mat-button-fn,`

- Rodar o comando local para instalar as libs

`npm i`

- Na sequência rodar o seguinte comando para instalar a lib

`npm run build:local`

- Incluir novamente no package.json a referência da lib (EM CASO DE CLONE INICIAL)

`@finchildren/mat-button-fn": "file:dist/mat-button-fn,`


## Inicialização do projeto

- Rodar o seguinte comando

`npm start`


## Publicação do projeto npm

### Apenas na primeira vez

- Ir até a pasta da lib projects\mat-button-fn
- Tornar o projeto local publico, rodando o seguinte comando (APENAS NA PRIMEIRA VEZ QUE FOR PUBLICAR)

`npm publish --access=public`

### Quando não for a primeira vez

- Rodar o seguinte comando:

`npm run build:publish`