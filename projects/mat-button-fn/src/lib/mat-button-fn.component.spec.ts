import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatButtonFnComponent } from './mat-button-fn.component';

describe('MatButtonFnComponent', () => {
  let component: MatButtonFnComponent;
  let fixture: ComponentFixture<MatButtonFnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatButtonFnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatButtonFnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
