import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialCustomModule } from './modules/material.module';
import { MatButtonFnComponent } from './mat-button-fn.component';



@NgModule({
  declarations: [
    MatButtonFnComponent
  ],
  imports: [
    BrowserAnimationsModule,
    MaterialCustomModule
  ],
  exports: [
    MatButtonFnComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MatButtonFnModule { }
