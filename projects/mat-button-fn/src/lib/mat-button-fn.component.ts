import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mat-button-fn',
  templateUrl: './mat-button-fn.component.html',
  styleUrls: ['./mat-button-fn.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MatButtonFnComponent implements OnInit {

  @Input() type: string = 'basic';

  constructor() { }

  ngOnInit(): void {

  }

}
