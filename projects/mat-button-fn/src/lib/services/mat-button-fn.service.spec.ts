import { TestBed } from '@angular/core/testing';

import { MatButtonFnService } from './mat-button-fn.service';

describe('MatButtonFnService', () => {
  let service: MatButtonFnService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MatButtonFnService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
